# Data handling
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

# Bokeh libraries
from bokeh.io import output_file, output_notebook
from bokeh.plotting import figure, show
from bokeh.models import ColumnDataSource
from bokeh.layouts import row, column, gridplot
from bokeh.models.widgets import Tabs, Panel


# Determine where the visualization will be rendered
output_file('weather.html',title='California Weather Data')


# My x-y coordinate data
rawdata = pd.read_csv('weatherdata.csv', delimiter = ',', parse_dates=['DATE'])

# Create table with only the data we want to use
weather = (rawdata
    [(rawdata['NAME'] == 'LOS ANGELES DOWNTOWN USC, CA US') |
    (rawdata['NAME'] == 'SACRAMENTO EXECUTIVE AIRPORT, CA US')]
    .loc[:, ['NAME', 'DATE', 'AWND', 'PRCP', 'TMAX', 'TMIN']]
    .sort_values(['NAME','DATE'])
    )
# moving average
weather['TMAX'] = weather.TMAX.rolling(window=65).mean()
weather['TMIN'] = weather.TMIN.rolling(window=65).mean()

#calculated differential
weather['TDIF'] = weather['TMAX'] - weather['TMIN']

# Isolate the data for the Sac and LA
sac_data = weather[weather['NAME'] == 'SACRAMENTO EXECUTIVE AIRPORT, CA US']
la_data = weather[weather['NAME'] == 'LOS ANGELES DOWNTOWN USC, CA US']

# Create a ColumnDataSource object for each city
sac_cds = ColumnDataSource(sac_data)
la_cds = ColumnDataSource(la_data)


# Set up the figure(s)
# Create a figure with no toolbar and axis ranges of [0,3]
# Create and configure the figures
highfig = figure(x_axis_type='datetime',
    plot_height=600, plot_width=1400,
    title='Temperature Highs',
    x_axis_label='Date', y_axis_label='Temp (C)',
    toolbar_location='below')
lowfig = figure(x_axis_type='datetime',
    plot_height=600, plot_width=1400,
    title='Temperature Lows',
    x_axis_label='Date', y_axis_label='Temp (C)',
    toolbar_location='below')
diffig = figure(x_axis_type='datetime',
    plot_height=600, plot_width=1400,
    title='Temperature Differences',
    x_axis_label='Date', y_axis_label='Temp (C)',
    toolbar_location='below')

# High Temp
highfig.line('DATE', 'TMAX', 
    color='#CE1141', legend='SAC_HIGH', 
    source=sac_cds, line_width=5)
highfig.line('DATE', 'TMAX', 
    color='#FF4500', legend='LA_HIGH', 
    source=la_cds, line_width=5, line_dash='dashed')
highfig.legend.location = 'top_left'


# Low Temp
lowfig.line('DATE', 'TMIN', 
    color='#800080', legend='SAC_LOW', 
    source=sac_cds, line_width=5)
lowfig.line('DATE', 'TMIN', 
    color='#006BB6', legend='LA_LOW', 
    source=la_cds, line_width=5, line_dash='dashed')
lowfig.legend.location = 'top_left'


# Diff Temp
diffig.line('DATE', 'TDIF', 
    color='#000000', legend='SAC_DIF', 
    source=sac_cds, line_width=5)
diffig.line('DATE', 'TDIF', 
    color='#808080', legend='LA_DIF', 
    source=la_cds, line_width=5, line_dash='dashed')
diffig.legend.location = 'top_left'



# Create panels using the figures
high_panel = Panel(child=highfig, title='High Temperatures')
low_panel = Panel(child=lowfig, title='Low Temperatures')
dif_panel = Panel(child=diffig, title='Difference in High/Low Temperatures')

# Add panels to tabs
tabs = Tabs(tabs=[high_panel, low_panel, dif_panel])

# Show the tabbed layout
show(tabs)